'''
Created on Jul 31, 2019

@author: JIgnacio
'''

import csv
import zipfile
import sys
import os
import pandas as pd
import xlrd, xlwt
from xlutils.copy import copy
from zipfile import ZIP_DEFLATED


plate_well_positions_vertical = [
        'A01','B01','C01','D01','E01','F01','G01','H01',
        'A02','B02','C02','D02','E02','F02','G02','H02',
        'A03','B03','C03','D03','E03','F03','G03','H03',
        'A04','B04','C04','D04','E04','F04','G04','H04',
        'A05','B05','C05','D05','E05','F05','G05','H05',
        'A06','B06','C06','D06','E06','F06','G06','H06',
        'A07','B07','C07','D07','E07','F07','G07','H07',
        'A08','B08','C08','D08','E08','F08','G08','H08',
        'A09','B09','C09','D09','E09','F09','G09','H09',
        'A10','B10','C10','D10','E10','F10','G10','H10',
        'A11','B11','C11','D11','E11','F11','G11','H11',
        'A12','B12','C12','D12','E12','F12','G12','H12'
    ]

plate_well_positions_horizontal = [
        'A01','A02','A03','A04','A05','A06','A07','A08','A09','A10','A11','A12',
        'B01','B02','B03','B04','B05','B06','B07','B08','B09','B10','B11','B12',
        'C01','C02','C03','C04','C05','C06','C07','C08','C09','C10','C11','C12',
        'D01','D02','D03','D04','D05','D06','D07','D08','D09','D10','D11','D12',
        'E01','E02','E03','E04','E05','E06','E07','E08','E09','E10','E11','E12',
        'F01','F02','F03','F04','F05','F06','F07','F08','F09','F10','F11','F12',
        'G01','G02','G03','G04','G05','G06','G07','G08','G09','G10','G11','G12',
        'H01','H02','H03','H04','H05','H06','H07','H08','H09','H10','H11','H12'
    ]

infinium_plate_well_format = [
        'A01','E02','G01','C03','B01','F02','H01','D03','C01','G02','A02','E03',
        'D01','H02','B02','F03','E01','A03','C02','G03','F01','B03','D02','H03',
        'A04','E05','G04','C06','B04','F05','H04','D06','C04','G05','A05','E06',
        'D04','H05','B05','F06','E04','A06','C05','G06','F04','B06','D05','H06',
        'A07','E08','G07','C09','B07','F08','H07','D09','C07','G08','A08','E09',
        'D07','H08','B08','F09','E07','A09','C08','G09','F07','B09','D08','H09',
        'A10','E11','G10','C12','B10','F11','H10','D12','C10','G11','A11','E12',
        'D10','H11','B11','F12','E10','A12','C11','G12','F10','B12','D11','H12'
    ]

sentrix_wells = [
        'R01C01', 'R01C02', 'R02C01', 'R02C02', 'R03C01', 'R03C02', 'R04C01', 'R04C02',
        'R05C01', 'R05C02', 'R06C01', 'R06C02', 'R07C01', 'R07C02', 'R08C01', 'R08C02',
        'R09C01', 'R09C02', 'R10C01', 'R10C02', 'R11C01', 'R11C02', 'R12C01', 'R12C02'
    ]

miseq_control_ids = []
miseq_control_names = []

class data:
    '''
    class to export sample metadata (in plates) to vendor files
    '''

    def __init__(self, sample_data, plate_data, experiment_name=None, uuid_header='uuid', direction='vertical',
                 check_positions=['H11','H12'], plate_well_position_header='plate_well_position',
                 sample_name_header='sample_name', plate_name_header='plate_name', sample_barcode_header='sample_barcode',
                 plate_barcode_header='plate_barcode'):
        self.sample_data = sample_data
        self.plate_data = plate_data
        self.experiment_name = experiment_name
        self.uuid_header = uuid_header
        self.direction = direction
        self.check_positions= check_positions
        self.plate_well_position_header = plate_well_position_header
        self.sample_name_header = sample_name_header
        self.plate_name_header = plate_name_header
        self.sample_barcode_header = sample_barcode_header
        self.plate_barcode_header = plate_barcode_header
        self.number_of_samples = len(sample_data)
        self.plate_well_positions = plate_well_positions_vertical if \
            direction == 'vertical' else plate_well_positions_horizontal

    # Exports Intertek format
    def intertek(self, intertek_template_file, outfile):
        rb = xlrd.open_workbook(intertek_template_file, formatting_info=True)
        wb = copy(rb)
        ws1 = wb.get_sheet(3)
        ws1_col_start = 1
        ws1_row_start = 12
        df = self.sample_data.reset_index(drop=True)
        plate_df = self.plate_data.reset_index(drop=True)

        tmp_df = df
        tmp_df.fillna('', inplace=True) # Prevents writing null values to excel

        # Write plate information
        for index, row in plate_df.iterrows():
            ws1.write(ws1_row_start + index, ws1_col_start, row[self.plate_name_header])
            ws1.write(ws1_row_start + index, ws1_col_start + 1, row[self.plate_barcode_header])

        # Write sample information
        ws2 = wb.get_sheet(4)
        ws2_col_start = 1
        ws2_row_start = 15

        for index, row in tmp_df.iterrows():
            ws2.write(ws2_row_start + index, ws2_col_start, row[self.sample_name_header])
            ws2.write(ws2_row_start + index, ws2_col_start + 1, row[self.plate_name_header])
            ws2.write(ws2_row_start + index, ws2_col_start + 2, row[self.plate_well_position_header])
            ws2.write(ws2_row_start + index, ws2_col_start + 3, row[self.sample_barcode_header])
            ws2.write(ws2_row_start + index, ws2_col_start + 4, row[self.plate_barcode_header])

        # Write plate layout
        ws3 = wb.get_sheet(5)
        ws3_col_start = 1
        ws3_row_start = 10

        idx = self.get_plate_dict(tmp_df)

        for i, x in self.plate_data.iterrows():
            p = x[self.plate_name_header]
            ws3.write(ws3_row_start + 11 * i, ws3_col_start, p)
            ws3 = self.format_to_plate(idx.get(p), ws3, ws3_row_start + 1 + 11 * i, ws3_col_start)

        wb.save(outfile + '.intertek.xls')

    def get_plate_dict(self, df):
        groups = self.sample_data[self.plate_name_header].unique()
        idx = dict(map(lambda x: (x, df.loc[df[self.plate_name_header] == x]), groups))
        return idx

    # Format samples as plates
    def format_to_plate(self, df, ws, row, col):
        row_dict = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7}
        for index, rows in df.iterrows():
            y = row_dict.get(rows[self.plate_well_position_header][:1]) + 1
            x = int(rows[self.plate_well_position_header][1:]) - 1
            ws.write(row + y, col + x, rows[self.sample_name_header])
        return ws

    # Make a miseq csv file for a single run (384 samples)
    def miseq_file(self, df, miseq_template_file, outfile, groups):
        exp_id = self.experiment_name
        idx = dict(map(lambda x: (x, df.loc[df[self.plate_name_header] == x]), groups))

        tmp_df = pd.DataFrame()
        for group in groups:
            tmp_df = tmp_df.append(self.reorder_samples(idx.get(group), self.plate_well_positions))

        import datetime as dt
        exp_name = exp_id
        date_row = 2
        assay_row = 5
        data_row_start = 22
        ofile = open(outfile, 'wb')
        writer = csv.writer(ofile)
        number_samples = tmp_df.shape[0]
        with open(miseq_template_file, 'rb') as miseq_csv:
            reader = csv.reader(miseq_csv)
            for row in range(data_row_start):
                if row == date_row:
                    new_row = reader.next()
                    new_row[1] = str(dt.date.today())
                    writer.writerow(new_row)
                elif row == assay_row:
                    new_row = reader.next()
                    new_row[1] = exp_name
                    writer.writerow(new_row)
                else:
                    writer.writerow(reader.next())
            row_index = 0
            sample_index = 0
            for row in reader:
                if sample_index < number_samples:
                    row_data = tmp_df.iloc[sample_index]
                    row[0] = str(row_data['sample_barcode'])
                    row[1] = str(row_data['sample_name'])
                    row[2] = str(row_data['plate_name'])
                    sample_index = sample_index + 1
                writer.writerow(row)
                row_index = row_index + 1

        ofile.close()

    # Export miseq file
    def miseq(self, miseq_template_file, outfile,  group_size=4):
        df = self.sample_data

        tmp_df = df
        tmp_df.fillna('', inplace=True) # Prevents writing null values to excel

        plates = self.sample_data[self.plate_name_header].unique()
        number_plates = len(plates)
        number_splits = (number_plates - 1) // group_size + 1
        groups = map(lambda x: plates[group_size * x: group_size * (x + 1)], range(number_splits))
        group_dict = dict(map(lambda x: (x, tmp_df.iloc[[i for i, r in tmp_df.iterrows() if
                                   r[self.plate_name_header] in set(groups[x])]]), range(number_splits)))

        miseq_zip = zipfile.ZipFile(outfile + '.miseq.zip', 'w', ZIP_DEFLATED)

        for group, df_group in group_dict.items():
            filename = outfile + '.miseq.' + str(group + 1) + '.csv'
            self.miseq_file(df_group, miseq_template_file, filename, groups[group])
            miseq_zip.write(filename, os.path.basename(filename))
            os.remove(filename)

        miseq_zip.close()

    # Export infinium file
    def infinium(self, infinium_template_file, sentrix_barcodes, outfile):
        df = self.sample_data
        plate_well_position = self.sample_data[self.plate_well_position_header]

        groups = df[self.plate_name_header].unique()
        number_plates = len(groups)
        idx = dict(map(lambda x: (x, df.loc[df[self.plate_name_header] == x]), groups))

        tmp_df = pd.DataFrame()
        for group in groups:
            tmp_df = tmp_df.append(self.reorder_samples(idx.get(group), self.plate_well_positions))

        tmp_df.fillna('', inplace=True)

        number_samples_per_plate = len(plate_well_position)
        number_barcodes = number_plates * 4

        barcodes = []

        if sentrix_barcodes != None:
            with open(sentrix_barcodes) as f:
                barcodes = f.read().splitlines()

        barcode_diff = number_barcodes - len(barcodes)
        if barcode_diff > 0:
            for i in range(barcode_diff):
                barcodes.append('')

        data_row_start = 11
        ofile = open(outfile + '.infinium.csv', 'wb')
        writer = csv.writer(ofile)

        with open(infinium_template_file, 'rb') as infinium_csv:
            reader = csv.reader(infinium_csv)
            for row_idx in range(data_row_start):
                row = reader.next()
                writer.writerow(row)

        for plate in range(number_plates):
            df_group = tmp_df.iloc[plate * number_samples_per_plate:(plate + 1) * number_samples_per_plate]
            df_group = self.reorder_samples(df_group, infinium_plate_well_format)
            for idx, row_data in df_group.iterrows():
                row = [''] * 6
                barcode_idx = (plate * 96 + idx) // 24
                row[0] = barcodes[barcode_idx]
                row[1] = sentrix_wells[idx % 24]
                row[2] = str(row_data[self.sample_barcode_header])
                row[3] = str(row_data[self.sample_name_header])
                row[4] = str(row_data[self.plate_name_header])
                row[5] = str(row_data[self.plate_well_position_header])
                writer.writerow(row)

        ofile.close()

    def reorder_samples(self, df, all_well_positions):
        plates = df[self.plate_name_header].unique()
        plate_well_position = all_well_positions
        number_samples = df.shape[0]
        number_samples_per_plate = len(plate_well_position)
        number_plates = (number_samples - 1) // number_samples_per_plate + 1
        new_df_rows = number_plates * number_samples_per_plate
        plate_names = []

        for plate in plates:
            plate_names = plate_names + [plate] * number_samples_per_plate

        new_df = pd.DataFrame(data={
            'sequence': range(new_df_rows),
            'plate_uuid': [''] * new_df_rows,
            self.plate_well_position_header: plate_well_position * number_plates,
            self.uuid_header: [''] * new_df_rows,
            self.sample_barcode_header: [''] * new_df_rows,
            self.sample_name_header: [''] * new_df_rows,
            self.plate_name_header: plate_names,
            self.plate_barcode_header: [''] * new_df_rows,
        }, columns=df.columns, dtype=object)

        plate_index = 0
        for plate in plates:
            current_df = df[df['plate_name'] == plate]
            for index, rows in current_df.iterrows():
                reorder_index = plate_well_position.index(rows[self.plate_well_position_header])
                new_df.loc[(plate_index * number_samples_per_plate) + reorder_index] = rows
            plate_index = plate_index + 1
        return new_df