import pandas as pd
import export as xp

#Set working directory 
wd="C:/Users/jignacio/workspace/SimpleTracker/SimpleTracker/"

#set params
intertek_template_file=wd+'export/intertek.xls'
miseq_template_file = 'export/miseq.csv'
infinium_template_file = 'export/infinium.csv'
sentrix_barcodes = 'export/sentrix.txt'
outfile=wd+'export/out'
well_position='well_position'

#creating input pandas dataframe
sample_data = pd.read_csv(wd+"export/test1_samples.txt",sep='\t',dtype='str', comment='#')
plate_data = pd.read_csv(wd+"export/test1_plates.txt",sep='\t',dtype='str', comment='#')

exp = xp.data(sample_data,plate_data,plate_well_position_header=well_position)
exp.intertek(intertek_template_file,outfile)
exp.miseq(miseq_template_file,outfile,group_size=384)
exp.infinium(infinium_template_file,sentrix_barcodes,outfile)
